﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculator_console
{
    public class Program
    {
        public static Func<double, double, double> Divide = (v1, v2) => v1 / v2;
        public static Func<double, double, double> Add = (v1, v2) => v1 + v2;
        public static Func<double, double, double> Multiply = (v1, v2) => v1 * v2;
        public static Func<double, double, double> Subtract = (v1, v2) => v1 - v2;
        public static Func<string, char> GetFirstLetter = (str) => str[0];
        static void Main(string[] args)
        {
            double tal1 = 0;
            double tal2 = 0;
            double resultat = 0;
            string operation = "";

            Console.WriteLine("Vælg tal1");
            tal1 = double.Parse(Console.ReadLine());

            Console.WriteLine("Vælg tal2");
            tal2 = double.Parse(Console.ReadLine());

            Console.WriteLine("Vælg operation (+,-,*,/)");
            operation = Console.ReadLine();

            if (operation == "+")
            {
                resultat = Add(tal1, tal2);
            }
            if (operation == "-")
            {
                resultat = Subtract(tal1, tal2);
            }
            if (operation == "*")
            {
                resultat = Multiply(tal1, tal2);
            }
            if (operation == "/")
            {
                resultat = Divide(tal1, tal2);
            }

            Console.WriteLine("Resultat er " + resultat);
            Console.ReadKey();
            Console.WriteLine(Subtract(5, 1));
        }
    }
}
